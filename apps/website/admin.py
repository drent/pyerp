from django.contrib import admin
from .submodels.website_config import WebsiteConfig

admin.site.register(WebsiteConfig)

